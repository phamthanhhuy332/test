package com.example.myfile.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myfile.utils.FileUtils

class FileRepository(private val context: Context) : ViewModelProvider.Factory {
    fun getImages() = FileUtils.getImages(context)

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FileViewModel::class.java)) {
            return FileViewModel(FileRepository(context = context)) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}