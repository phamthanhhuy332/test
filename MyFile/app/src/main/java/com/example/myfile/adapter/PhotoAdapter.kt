package com.example.myfile.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.myfile.R
import com.example.myfile.databinding.PhotoItemBinding
import com.example.myfile.extension.toUriExtension

class PhotoAdapter(private val photos: List<String>, private val onClick: (String) -> Unit) :
    RecyclerView.Adapter<PhotoAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val binding: PhotoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photo: String, onClick: (String) -> Unit) {
            Glide.with(binding.root)
                .load(photo.toUriExtension())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_baseline_image_24)
                .error(R.drawable.ic_baseline_error_24)
                .into(binding.imgPhoto)

            binding.root.setOnClickListener { onClick(photo) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PhotoItemBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) =
        holder.bind(photos[position], onClick)

    override fun getItemCount(): Int = photos.size
}