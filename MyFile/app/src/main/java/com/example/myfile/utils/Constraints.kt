package com.example.myfile.utils

object Constraints {
    val PERMISSION_READ_EXTERNAL = android.Manifest.permission.READ_EXTERNAL_STORAGE
    val ALL_PERMISSION: Array<String> = arrayOf(PERMISSION_READ_EXTERNAL)
}