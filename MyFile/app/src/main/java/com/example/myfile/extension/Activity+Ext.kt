package com.example.myfile.extension

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

fun Activity.requestPermission(permissions: Array<String>) {
    ActivityCompat.requestPermissions(
        this,
        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
        1
    );
}

fun Activity.checkPermission(permissions: Array<String>): Boolean {
    var isGrantedAll = true

    for (permission in permissions) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            isGrantedAll = false
        }
    }

    return isGrantedAll
}