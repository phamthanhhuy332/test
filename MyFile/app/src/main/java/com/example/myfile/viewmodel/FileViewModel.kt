package com.example.myfile.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData

class FileViewModel(private val repository: FileRepository) : ViewModel() {

    fun getImages() = liveData {
        emit(repository.getImages())
    }
}