package com.example.myfile.utils

import android.content.Context
import android.provider.MediaStore

object FileUtils {

    fun getImages(context: Context): MutableList<String> {
        val contentResolver = context.contentResolver
        val images = mutableListOf<String>()

        val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = null
        val selection = null
        val selectionArgs = null
        val sort = "${MediaStore.Images.ImageColumns.DATE_TAKEN}" //DESC LIMIT 2 OFFSET 2"

        val cursor = contentResolver.query(uri, projection, selection, selectionArgs, sort)

        cursor?.let {
            if (it.moveToFirst()) {
                //index folder -> la ten folder chua file, neu file nam trong thu muc goc thi se null
                val indexFolder =
                    it.getColumnIndex(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME)

                //index path -> la duong dan den file
                val indexPath = it.getColumnIndex(MediaStore.Images.ImageColumns.DATA)

                do {
                    images.add(it.getString(indexPath))
                } while (it.moveToNext());
            }
        }

        return images;
    }
}