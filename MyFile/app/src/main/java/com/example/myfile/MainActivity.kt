package com.example.myfile

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myfile.adapter.PhotoAdapter
import com.example.myfile.databinding.ActivityMainBinding
import com.example.myfile.extension.checkPermission
import com.example.myfile.extension.requestPermission
import com.example.myfile.extension.show
import com.example.myfile.utils.Constraints
import com.example.myfile.viewmodel.FileRepository
import com.example.myfile.viewmodel.FileViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: FileViewModel
    private val TAG = "PhamThanhHuy09"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.apply {
            lifecycleOwner = this@MainActivity
        }
        setContentView(binding.root)
        setupViewModel()
        setupPermission()

    }

    private fun setupViewModel() {
        val repository = FileRepository(context = applicationContext)
        viewModel = ViewModelProvider(this, repository).get(FileViewModel::class.java)
    }

    private fun setupPermission() {
        requestPermission(Constraints.ALL_PERMISSION)

        if (checkPermission(Constraints.ALL_PERMISSION)) {
            viewModel.getImages()
                .observe(this, { setupFiles(it) })
        }
    }

    private fun onClick(photo: String) {
        show(photo)
    }

    private fun setupFiles(photos: List<String>) {
        val adapter = PhotoAdapter(photos = photos) { onClick(it) }
        val layout = GridLayoutManager(this, 3)
        binding.rcvPhotos.adapter = adapter
        binding.rcvPhotos.layoutManager = layout
        binding.rcvPhotos.setHasFixedSize(true)
    }
}